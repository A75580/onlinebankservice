package com.onlinebank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlinebank.model.OnlineBankAccount;

@Repository
public interface BankRepository extends JpaRepository<OnlineBankAccount, Long> {

}
