package com.onlinebank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlinebank.model.TransactionHistory;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionHistory, Long> {

}
