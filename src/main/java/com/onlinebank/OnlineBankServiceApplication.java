package com.onlinebank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

import com.onlinebank.config.RibbonConfiguration;

@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name="onlinebankservice",configuration = RibbonConfiguration.class)
public class OnlineBankServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineBankServiceApplication.class, args);
	}

}
