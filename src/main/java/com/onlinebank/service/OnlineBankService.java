package com.onlinebank.service;

import java.util.List;

import com.onlinebank.model.Customer;
import com.onlinebank.model.TransactionHistory;

public interface OnlineBankService {

	public Customer saveOrUpdateCustomer(Customer customer);

	public TransactionHistory makepayment(TransactionHistory transhistory) throws Exception;

	public List<TransactionHistory> getstatement(long userid);

}

